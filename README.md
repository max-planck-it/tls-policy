# tls-policy

This is an attempt to maintain a list of MTAs and their TLS capabilities.
Due to my affinity to Postfix we will start out with the format provided by this software.
Please feel free to add any other format, preferably accompanied by a conversion script.

## Note

Please be aware using this list will add administrative overhead if MX records should change.

This rarely happens but you should still make sure to keep the list up to date by
using something like a cron job.

To keep track of changes in the repository, you may subscribe to those by
registering at GitLab.com, and change the notification setting to *Watch*.

## Motivation

The goal is to ensure to send email over secure channels only.
Unfortunately, this cannot be done by default, as in reality email
delivery would fail to some hosts, not supporting STARTTLS, though the
exceptions are few.

For MPG organizations it is required that all SMTP servers offer TLS.
So, it would be obvious to verify certificates. Unfortunately,
that is not possible, either, as some certificates are invalid.

DNSSEC/DANE would solve this problem partly, but it’s not very common.
Moreover, if no TLSA records are found, the original problem persists.

As a result a list needs to be maintained.
